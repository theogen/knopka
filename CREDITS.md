# Credits

Game (unexpectedly) developed by Theogen Ratkin for [Unexpected
Jam](https://itch.io/jam/unexpectedjam).

## Third party assets

- [Font "Montserrat"](https://fonts.google.com/specimen/Montserrat) by Julieta
Ulanovsky, Sol Matas, Juan Pablo del Peral, Jacques Le Bailly

- [Song "Charcoal"](https://freemusicarchive.org/music/Chad_Crouch/Ambient_Atmospheres/Charcoal) by Chad Crouch

- [Button press sound](https://freesound.org/people/GreekIrish/sounds/254713/) by GreekIrish

- [Wind sound](https://freesound.org/people/wertstahl/sounds/409273/) by wertstahl

- [Supersonic effect](https://freesound.org/people/Robinhood76/sounds/324704/) by Robinhood76

- [Boat sound](https://freesound.org/people/devy32/sounds/441223/) by devy32

- [Growing tree sound](https://freesound.org/people/Kinoton/sounds/494071/) by Kinoton

- [UI click sound](https://freesound.org/people/Breviceps/sounds/448086/) by Breviceps
